import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { DataServiceService } from 'src/app/services/data-service.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
	
  employeeList:any = [
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 1
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 2
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 3
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 4
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 5
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 6
                        },
                        {name: 'Imdadul Haque',
                         email: 'imdadul@gmail.com',
                         address: '',
                         birthdate: '1990-03-12',
                         designation: '',
                         id: 7
                        },

                      ];
  profile: any;
  showLoader:Boolean = false;
  birthDate: any = '1990-03-12';

  constructor(private dataService: DataServiceService, 
              private router: Router) { 
    this.getDataUser();
  }

  ngOnInit() {
    // this.getEmployeeList();
  	
  }

  getEmployeeList() {
    this.showLoader = true;
    this.dataService.getAllEmployees().subscribe(data => {
      this.employeeList = data; 
      this.showLoader = false;
    },
    error => {                
      console.log("Error occured!" + error);
    });
  }

  async getDataUser() {
    
    await this.dataService.getUser()
      .subscribe(res => {
        console.log(res);
      }, err => {
        console.log(err);
      });
  }

  loadUser() {
    this.dataService.getUser().subscribe(data => this.profile = data);
    console.log(this.profile);
  }


}
