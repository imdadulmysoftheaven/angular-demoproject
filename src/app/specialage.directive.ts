import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appSpecialage]'
})
export class SpecialageDirective {

  constructor() { }

  @Input() dob: string;
  constructor(private elementRef: ElementRef) {
  }
  ngOnInit() {
	let todayDate = new Date(Date.parse(Date()));
	console.log(this.dob);
	let d1 = Date.now();
	let d2 = new Date(1990,5,22).getTime();
  	let year = this.yearsDiff(d1, d2);

	console.log(year);

  }

  yearsDiff(d1, d2) {
  	let date1 = new Date(d1);
  	let date2 = new Date(d2);
  	let yearsDiff = date1.getFullYear() - date2.getFullYear();
  	return yearsDiff;
  }

}
