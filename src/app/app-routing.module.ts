import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';

const routes: Routes = [
  	{ path: 'employee-list-component', component: EmployeeListComponent },
	{ path: 'add-employee-component', component: AddEmployeeComponent },
  	{ path: 'employee-details-component/:id', component: EmployeeDetailsComponent },
  	{ path: '',   redirectTo: '/employee-list-component', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
