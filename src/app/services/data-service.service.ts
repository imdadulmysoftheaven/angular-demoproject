import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json',
                              'Access-Control-Allow-Origin': 'http://n3logic.com',
                              'content-type': 'application/x-www-form-urlencoded'
                            })
  };

  private apiUrl = "https://jsonplaceholder.typicode.com/users";

	private baseUrl = 'http://n3logic.com/ems_bs55/api';

  constructor(private http: HttpClient) { }

  getAllEmployees(): Observable<any[]> {
    const headers = { 'content-type': 'application/json'}  
    console.log(headers)
    console.log(this.apiUrl);
    return this.http.get<any[]>(this.baseUrl+'/posts',{'headers':headers})
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getDataUser(): Observable<any> {
    // return this.http.get(apiUrl, httpOptions).pipe(
    return this.http.get(this.apiUrl).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getEmployeeList(): Observable<any> {
    return this.http.get(this.baseUrl+"/employee-list").pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getAddEmployee(): Observable<any> {
    return this.http.get(this.baseUrl+"/add-employee").pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getEditEmployee(): Observable<any> {
    return this.http.get(this.baseUrl+"/update-employee").pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getDeleteEmployee(id): Observable<any> {
    return this.http.get(this.baseUrl+"/delete-employee/"+id).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  getUser() {
    return this.http.get(`http://n3logic.com/ems_bs55/api/employee-list`);
  }

}
